INTRODUCTION
------------

[Bibliography & Citation - PubMed](https://www.drupal.org/project/bibcite_pubmed) module provides PubMed import and
lookup functionality for the [Bibliography & Citation](https://www.drupal.org/project/bibcite) module.

 * For a full description of the module, visit the project page:  
   https://www.drupal.org/project/bibcite_pubmed

 * To submit bug reports and feature suggestions, or to track changes:  
   https://www.drupal.org/project/issues/bibcite_pubmed


REQUIREMENTS
------------

This module requires the following modules:

 * [Bibliography & Citation](https://www.drupal.org/project/bibcite)


INSTALLATION
------------

 * Install as you would normally install a contributed Drupal module. See:
   https://www.drupal.org/docs/8/extending-drupal/installing-contributed-modules
   for further information.


MAINTAINERS
-----------

Current maintainers:

 * Anton Shubkin (antongp) - https://www.drupal.org/u/antongp
 * adci_contributor - https://www.drupal.org/u/adci_contributor

This project has been sponsored by [ADCI Solutions](https://www.adcisolutions.com/)
